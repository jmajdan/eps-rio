package cz.wincor.eps;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cz.wincor.eps.engine.Engine;

public class Application 
{


    private static final Logger logger = LogManager.getLogger(Application.class);
    private static SessionFactory factory; 
    
    public static List<ConfigurationParameter> list( )
    {
        Session session = factory.openSession();
        Transaction tx = null;
        List<ConfigurationParameter> configs = null;
        try
        {
           tx = session.beginTransaction();
           configs = session.createQuery("FROM ConfigurationParameter").list();
//           for (Iterator iterator = employees.iterator(); iterator.hasNext();){
//              Employee employee = (Employee) iterator.next(); 
//              System.out.print("First Name: " + employee.getFirstName()); 
//              System.out.print("  Last Name: " + employee.getLastName()); 
//              System.out.println("  Salary: " + employee.getSalary()); 
//           }
           tx.commit();
        } 
        catch (HibernateException e) 
        {
           if (tx!=null) tx.rollback();
           e.printStackTrace(); 
        } 
        finally 
        {
           session.close(); 
        }
        
        return configs;
     }
    
    public static void main(String[] args) 
    {
        logger.info("Eps is starting ...");
        logger.info("Startinig initialization ... ");
        
        ApplicationContext context = new ClassPathXmlApplicationContext("app.spring.xml");
        Engine engine = context.getBean("flow-engine", Engine.class);

        
        try 
        {
           factory = new Configuration().configure("hibernate.cfg.xml").
//                     addPackage("com.xyz") //add package if used.
//                     addAnnotatedClass(Employee.class).
                     buildSessionFactory();
        } 
        catch (Throwable ex) 
        { 
           logger.error("Failed to create sessionFactory object: " + ex);
           throw new ExceptionInInitializerError(ex); 
        }
        
        logger.info("Initialization has been finished ... ");

        List<ConfigurationParameter> configs = list();
        for (ConfigurationParameter config : configs)
        {
            engine.run(1, config.getPropKey());
        }
        
        logger.info("Eps is stopping ... ");
    }

}

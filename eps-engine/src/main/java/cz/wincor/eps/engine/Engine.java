package cz.wincor.eps.engine;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Engine 
{
    private final Logger logger = LogManager.getLogger(getClass());
    
    public void run(int i, String msg)
    {
        logger.info("Flow engine: starting ...");
        
        for (int j = 0; j < i; ++j)
        {
            System.out.println(msg);
        }
        
        logger.info("Flow engine: finishing ...");
    }

}
